country = 'India';
function getName(id){
    country = id;
}
function getCountry() {
    switch (country) {
        case "India":
            getIndiaTime();
            // setInterval(getIndiaTime, 1000);
            break;

        case "London":
            getLondonTime();
            // setInterval(getLondonTime, 1000);
            break;

        case "New York":
            getNewYorkTime();
            // setInterval(getNewYorkTime, 1000);
            break;

        case "Sydney":
            getSydneyTime();
            // setInterval(getSydneyTime, 1000);
            break;

        case "Pakistan":
            getPakistanTime();
            // setInterval(getPakistanTime);
            break;
    }
}
setInterval(getCountry,1000);

let getIndiaTime = () => {
    document.getElementById("time").innerHTML = "India : " + new Date().toLocaleString("en-US", { timeZone: 'Asia/Kolkata', dateStyle: 'full', timeStyle: 'long', hourCycle: 'h12' });
    // setInterval(getIndiaTime, 1000);
    // var setIndiaTime = setInterval(getIndiaTime, 1000);
    // clearInterval(setIndiaTime);
}

let getLondonTime = () => {
    document.getElementById("time").innerHTML = "London : " + new Date().toLocaleString("en-US", { timeZone: 'Europe/London', dateStyle: 'full', timeStyle: 'long', hourCycle: 'h12' });
    // setInterval(getIndiaTime, 1000);
    // var setLondonTime = setInterval(getLondonTime, 1000);
    // clearInterval(setLondonTime);
}

let getNewYorkTime = () => {
    document.getElementById("time").innerHTML = "New York : " +  new Date().toLocaleString("en-US", { timeZone: 'America/New_York', dateStyle: 'full', timeStyle: 'long', hourCycle: 'h12' });
    // setInterval(getNewYorkTime, 1000);
    // var setNewYorkTime = setInterval(getNewYorkTime, 1000);
    // clearInterval(setNewYorkTime);
}

let getSydneyTime = () => {
    document.getElementById("time").innerHTML = "Sydney : " + new Date().toLocaleString("en-US", { timeZone: 'Australia/Sydney', dateStyle: 'full', timeStyle: 'long', hourCycle: 'h12' });
    // setInterval(getSydneyTime);
    // var setSydneyTime = setInterval(getSydneyTime, 1000);
    // clearInterval(setSydneyTime);
}

let getPakistanTime = () => {
    document.getElementById("time").innerHTML = "Karachi : " +  new Date().toLocaleString("en-US", { timeZone: 'Asia/Karachi', dateStyle: 'full', timeStyle: 'long', hourCycle: 'h12' });
    // getPakistanTime(getPakistanTime);
    // var setPakistanTime = setInterval(getPakistanTime, 1000);
    // clearInterval(setPakistanTime);
}
